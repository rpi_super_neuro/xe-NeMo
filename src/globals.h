/**

	xe_Nemo -- A parallel discrete event simulation based neuromorphic hardware simulation model.

	@file globals.h

	@brief 


	@author	Mark Plagge
	@bug	

**/

/*

	Copyright © 2015-2018 Mark Plagge.


	
	The MIT License (MIT)
	
	Copyright (c) 2018 
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
	

*/


#ifndef GLOBALS_XE_NEMO_H
#define GLOBALS_XE_NEMO_H

#include <ross.h>


#ifdef TEST
#include "CuTest.h"
#endif

/** @defgroup types Typedef Vars
 * Typedefs to ensure proper types for the neuron parameters/mapping calculations
 */
/**@{  */

typedef uint_fast64_t ne_id_type; //!< id type is used for local mapping functions - there should be $n$ of them depending on CORE_SIZE
typedef int32_t ne_volt_type; //!< volt_type stores voltage values for membrane potential calculations
typedef int64_t ne_weight_type;//!< seperate type for synaptic weights.
typedef uint32_t ne_thresh_type;//!< Type for weights internal to the neurons.
typedef uint16_t ne_random_type;//!< Type for random values in neuron models.
typedef uint64_t ne_size_type; //!< size_type holds sizes of the sim - core size, neurons per core, etc.
typedef uint64_t ne_stat_type; //!< holds statistical values - per-neuron activity, spike counts, etc.

/**@}*/

/** @defgroup cmacros Global Dynamic Typing casting macros for file IO */
/** @{ */
#define ATOX(x) _Generic((x), \
double: atof,\
long: atol,\
int: atoi,\
float: atof

/**@}*/

/**
 *  * @defgroup gmacros Global Macros and Related Functions
 *Global Macros */
/**@{ */
/** TODO: Eventually replace this with generic macro and non-branching ABS code. */
#define IABS(a) (((a) < 0) ? (-a) : (a)) //!< Typeless integer absolute value function
/** TODO: See if there is a non-branching version of the signum function, maybe in MAth libs and use that. */
#define SGN(x) ((x > 0) - (x < 0)) //!< Signum function
#define DT(x) !(x) //!<Kronecker Delta function.
#define BINCOMP(s, p) IABS((s)) >= (p) //!< binary comparison for conditional stochastic evaluation


/** @} */

/** @defgroup timeFuncts Time Functions
  * Functions that manage big tick and little ticks
  */
/** @{ */

/** Macro for use within globals.
 Assumes that there is a tw_lp pointer called lp in the function it is used.
 */
#define NE_JITTER (tw_rand_unif(lp->rng) / 10000)

/**
 *  Gets the next event time, based on a random function. Moved here to allow for
 *  easier abstraction, and random function replacement.
 *
 *
 *  @param lp Reference to the current LP so that the function can see the RNG
 *
 *  @return a tw_stime value, such that \f$ 0 < t < 1 \f$. A delta for the next
 *  time slice.
 */
tw_stime ne_get_next_event_time(tw_lp *lp);
/**
 *  @brief  Given a tw_stime, returns the current big tick.
 *
 *  @param now current time
 *
 *  @return the current big tick time.
 */
tw_stime ne_get_current_big_tick(tw_stime now);

/**
 *  @brief  Given a tw_stime, returns the next big-tick that will happen
 *
 *  @param lp the lp asking for the next big tick.
 *  @param neuronID Currently unused. Reserved for future fine-grained neuron tick management.
 *
 *  @return Next big tick time.
 */
tw_stime ne_get_next_big_tick(tw_lp *lp, tw_lpid neuronID);


/**
 * @brief Returns the next heartbeat time delta for a synapese
 * Used in super-synapse methods.
 * @param lp
 * @return
 */
tw_stime ne_get_next_syn_heartbeat(tw_lp *lp);


/**@}*/

/**
 * String concat that returns a pointer to the very end of the string.
 * Example:
 * dest = 'abc\0\0\0\0' src = 'def'
 * result = 'abcdef\0', with the pointer now facing '\0'
 * @param dest destination string - must be large enough to hold itself and the source string
 * @param src 'source' string - appends to the dest string
 * @return a pointer to the end of the new string.
 */
char *ne_str_cat_end(char *dest, char *src);



#endif
