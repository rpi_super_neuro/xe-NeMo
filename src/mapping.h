/**

	xe_Nemo -- A parallel discrete event simulation based neuromorphic hardware simulation model.

	@file mapping.h

	@brief 


	@author	Mark Plagge
	@bug	

**/

/*

	Copyright © 2015-2018 Mark Plagge.


	
	The MIT License (MIT)
	
	Copyright (c) 2018 
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
	

*/


#ifndef MAPPING_XE_NEMO_H
#define MAPPING_XE_NEMO_H

#ifdef TEST
#include "CuTest.h"
#endif


/// This is a sample function with a doxygen description.
// void void_f(void);

#endif
